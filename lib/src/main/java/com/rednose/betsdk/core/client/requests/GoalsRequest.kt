package com.rednose.betsdk.core.client.requests

import com.rednose.betsdk.core.client.models.BAGoal
import retrofit2.http.GET
import retrofit2.http.Path

interface GoalsRequest {

    @GET("matches/{id}/goals")
    suspend fun matchGoals(@Path("id") id: String): List<BAGoal>
}