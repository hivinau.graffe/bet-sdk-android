package com.rednose.betsdk.core.client.datamanager

import com.rednose.betsdk.core.retrofit.Retrofit

class BetDataManager(private val retrofit: Retrofit) : DataManager {

    // MARK: - DataManager methods

    override fun <T> create(service: Class<T>?): T {
        return retrofit.create(service)
    }
}