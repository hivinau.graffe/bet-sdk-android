package com.rednose.betsdk.core.client

import com.rednose.betsdk.core.client.models.BABet
import com.rednose.betsdk.core.client.models.BAGoal
import com.rednose.betsdk.core.client.models.BAMatch
import com.rednose.betsdk.core.client.models.BAPro

interface Client {
    suspend fun bets(): List<BABet>
    suspend fun betMatches(betId: String): List<BAMatch>
    suspend fun matchPros(matchId: String): List<BAPro>
    suspend fun matchGoals(matchId: String): List<BAGoal>
}