package com.rednose.betsdk.core.client.models

interface Bet {
    val id: String?
    val locked: Boolean
    val priceToUnlock: Float
    val matchesCount: Int
}