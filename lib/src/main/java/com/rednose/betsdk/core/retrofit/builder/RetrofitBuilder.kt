package com.rednose.betsdk.core.retrofit.builder

import com.rednose.betsdk.core.retrofit.RetrofitWrapper

interface RetrofitBuilder {
    fun build(): RetrofitWrapper
}