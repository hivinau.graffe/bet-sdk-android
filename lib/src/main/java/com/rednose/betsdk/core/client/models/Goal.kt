package com.rednose.betsdk.core.client.models

interface Goal {
    val id: String?
    val scorer: String?
    val time: Int
    val teamId: String?
}