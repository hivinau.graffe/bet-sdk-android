package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BAGoal
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class BAGoalTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val goalString = """{"id":"b5d16697-471b-4ff5-b34e-5c50641eca46","scorer":"Illan Meslier (OG)","time":64,"team_id":"7c8e6980-938e-4f8e-92a6-b0bb303bae51"}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfGoal() {
        val goal = gson.fromJson(Constants.goalString, BAGoal::class.java)

        assertEquals(goal.id, "b5d16697-471b-4ff5-b34e-5c50641eca46")
        assertEquals(goal.scorer, "Illan Meslier (OG)")
        assertEquals(goal.time, 64)
        assertEquals(goal.teamId, "7c8e6980-938e-4f8e-92a6-b0bb303bae51")
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsGoalString() {
        val goal = BAGoal(
            "b5d16697-471b-4ff5-b34e-5c50641eca46",
            "Illan Meslier (OG)",
            64,
            "7c8e6980-938e-4f8e-92a6-b0bb303bae51"
        )

        val jsonString = gson.toJson(goal)

        assertEquals(jsonString, Constants.goalString)
    }
}
