package com.rednose.betsdk.core.client.requests.factory

import com.rednose.betsdk.core.client.datamanager.DataManager
import com.rednose.betsdk.core.client.requests.BetsRequest
import com.rednose.betsdk.core.client.requests.GoalsRequest
import com.rednose.betsdk.core.client.requests.MatchesRequest
import com.rednose.betsdk.core.client.requests.ProsRequest

class BetRequestFactory(private val dataManager: DataManager) :
    RequestFactory {

    // MARK: - RequestFactory methods

    override fun betsRequest(): BetsRequest {
        return dataManager.create(BetsRequest::class.java)
    }

    override fun matchesRequest(): MatchesRequest {
        return dataManager.create(MatchesRequest::class.java)
    }

    override fun prosRequest(): ProsRequest {
        return dataManager.create(ProsRequest::class.java)
    }

    override fun goalsRequest(): GoalsRequest {
        return dataManager.create(GoalsRequest::class.java)
    }
}