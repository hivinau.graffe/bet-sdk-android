<h1 align="center">
Bet Sdk
</h1>

[![pipeline status](https://gitlab.com/hivinau.graffe/bet-sdk-android/badges/develop/pipeline.svg)](https://gitlab.com/hivinau.graffe/bet-sdk-android/-/commits/develop)
[![coverage report](https://gitlab.com/hivinau.graffe/bet-sdk-android/badges/develop/coverage.svg)](https://gitlab.com/hivinau.graffe/bet-sdk-android/-/commits/develop)

# Contents

[[_TOC_]]

# Features

## List bets

You can find the dedicated documentation here: [/bets](docs/list-bets.md).

## List matches

You can find the dedicated documentation here: [/bets/\{id\}/matches](docs/list-matches.md).

## List pros

You can find the dedicated documentation here: [/matches/\{id\}/pros/](docs/list-pros.md).

## List goals

You can find the dedicated documentation here: [/matches/\{id\}/goals/](docs/list-goals.md).

## Get image

You can find the dedicated documentation here: [/images/\{image\}](docs/get-image.md).

# Supported OS & SDK version

- Android Studio 4.2
- Kotlin 1.4
- Gradle 4.2

# How to ?

## Installation

# About

## Author

Hivinau GRAFFE [hivinau.graffe@hotmail.fr](mailto:hivinau.graffe@hotmail.fr)

## License

This project is released under the MIT license. [See LICENSE](LICENSE) for details.
