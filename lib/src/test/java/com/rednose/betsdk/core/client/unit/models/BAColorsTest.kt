package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BAColors
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class BAColorsTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val colorsString = """{"background":"#ED1C24","primary":"#262122","secondary":"#FFFEFE","detail":"#FDF8F9"}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfBAColors() {
        val colors = gson.fromJson(Constants.colorsString, BAColors::class.java)

        assertEquals(colors.background, "#ED1C24")
        assertEquals(colors.primary, "#262122")
        assertEquals(colors.secondary, "#FFFEFE")
        assertEquals(colors.detail, "#FDF8F9")
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsColorsString() {
        val colors = BAColors(
            "#ED1C24",
            "#262122",
            "#FFFEFE",
            "#FDF8F9"
        )

        val jsonString = gson.toJson(colors)

        assertEquals(jsonString, Constants.colorsString)
    }
}
