package com.rednose.betsdk.core.client

import com.rednose.betsdk.core.client.models.BABet
import com.rednose.betsdk.core.client.models.BAGoal
import com.rednose.betsdk.core.client.models.BAMatch
import com.rednose.betsdk.core.client.models.BAPro
import com.rednose.betsdk.core.client.requests.factory.RequestFactory

class BetClient(private val requestFactory: RequestFactory): Client {

    // MARK: - Client methods

    override suspend fun bets(): List<BABet> {
        return requestFactory.betsRequest().bets()
    }

    override suspend fun betMatches(betId: String): List<BAMatch> {
        return requestFactory.matchesRequest().betMatches(betId)
    }

    override suspend fun matchPros(matchId: String): List<BAPro> {
        return requestFactory.prosRequest().matchPros(matchId)
    }

    override suspend fun matchGoals(matchId: String): List<BAGoal> {
        return requestFactory.goalsRequest().matchGoals(matchId)
    }
}