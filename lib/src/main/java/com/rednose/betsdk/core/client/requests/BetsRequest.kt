package com.rednose.betsdk.core.client.requests

import com.rednose.betsdk.core.client.models.BABet
import retrofit2.http.GET

interface BetsRequest {

    @GET("bets")
    suspend fun bets(): List<BABet>
}