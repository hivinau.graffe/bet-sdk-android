package com.rednose.betsdk.core.retrofit

interface Retrofit {
    fun <T> create(service: Class<T>?): T
}