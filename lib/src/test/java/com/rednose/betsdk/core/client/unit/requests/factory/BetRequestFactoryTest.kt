package com.rednose.betsdk.core.client.unit.requests.factory

import com.rednose.betsdk.core.client.datamanager.DataManager
import com.rednose.betsdk.core.client.requests.BetsRequest
import com.rednose.betsdk.core.client.requests.GoalsRequest
import com.rednose.betsdk.core.client.requests.MatchesRequest
import com.rednose.betsdk.core.client.requests.ProsRequest
import com.rednose.betsdk.core.client.requests.factory.BetRequestFactory
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
class BetRequestFactoryTest {

    // MARK: - Constants

    class Constants {
        companion object {
            val betsRequest = BetsRequest::class.java
            val matchesRequest = MatchesRequest::class.java
            val prosRequest = ProsRequest::class.java
            val goalsRequest = GoalsRequest::class.java
        }
    }

    // MARK: - Private properties

    private lateinit var tested: BetRequestFactory
    private val dataManager = mock<DataManager>()

    // MARK: - Lifecycle

    @Before
    fun setup() {
        tested = BetRequestFactory(dataManager)
    }

    // MARK: - Tests

    @Test
    fun dataManagerCreateBetsRequest_shouldBeCalled_whenBetsRequestCalled() {
        tested.betsRequest()

        verify(dataManager).create(Constants.betsRequest)
    }

    @Test
    fun dataManagerCreateMatchesRequest_shouldBeCalled_whenMatchesRequestCalled() {
        tested.matchesRequest()

        verify(dataManager).create(Constants.matchesRequest)
    }

    @Test
    fun dataManagerCreateProsRequest_shouldBeCalled_whenProsRequestCalled() {
        tested.prosRequest()

        verify(dataManager).create(Constants.prosRequest)
    }

    @Test
    fun dataManagerCreateGoalsRequest_shouldBeCalled_whenGoalsRequestCalled() {
        tested.goalsRequest()

        verify(dataManager).create(Constants.goalsRequest)
    }
}