package com.rednose.betsdk.core.client.models

interface Team {
    val id: String?
    val name: String?
    val image: String?
    val colors: Colors?
}