package com.rednose.betsdk.core.client.unit

import com.rednose.betsdk.core.client.BetClient
import com.rednose.betsdk.core.client.models.BABet
import com.rednose.betsdk.core.client.models.BAGoal
import com.rednose.betsdk.core.client.models.BAMatch
import com.rednose.betsdk.core.client.models.BAPro
import com.rednose.betsdk.core.client.requests.BetsRequest
import com.rednose.betsdk.core.client.requests.GoalsRequest
import com.rednose.betsdk.core.client.requests.MatchesRequest
import com.rednose.betsdk.core.client.requests.ProsRequest
import com.rednose.betsdk.core.client.requests.factory.RequestFactory
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*

@RunWith(MockitoJUnitRunner::class)
class BetClientTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val betId = "betId"
            const val matchId = "matchId"
            const val proId = "proId"
            const val goalId = "goalId"
        }
    }

    // MARK: - Private properties

    private lateinit var tested: BetClient
    private val betsRequest = mock<BetsRequest>()
    private val matchesRequest = mock<MatchesRequest>()
    private val prosRequest = mock<ProsRequest>()
    private val goalsRequest = mock<GoalsRequest>()
    private val requestFactory = mock<RequestFactory> {
        on { betsRequest() }.doReturn(betsRequest)
        on { matchesRequest() }.doReturn(matchesRequest)
        on { prosRequest() }.doReturn(prosRequest)
        on { goalsRequest() }.doReturn(goalsRequest)
    }

    // MARK: - Lifecycle

    @Before
    fun init() {
        tested = BetClient(requestFactory)
    }

    // MARK: - Tests: bets

    @Test
    fun requestFactoryBetsRequest_shouldBeCalled_whenBetsCalled(): Unit = runBlocking {
        tested.bets()

        verify(requestFactory).betsRequest()
    }

    @Test
    fun betsRequestBets_shouldBeCalled_whenBetsCalled(): Unit = runBlocking {
        val bets = mutableListOf(
            BABet(
                Constants.betId,
                false,
                0F,
                0
            )
        )
        betsRequest.stub {
            onBlocking { bets() }.doReturn(bets)
        }

        tested.bets()

        verify(betsRequest, atLeastOnce()).bets()
    }

    // MARK: - Tests: matches

    @Test
    fun requestFactoryMatchesRequest_shouldBeCalled_whenBetMatchesCalled(): Unit = runBlocking {
        tested.betMatches(Constants.betId)

        verify(requestFactory).matchesRequest()
    }

    @Test
    fun betsRequestBetMatches_shouldBeCalled_whenBetMatchesCalled(): Unit = runBlocking {
        val matches = mutableListOf(
            BAMatch(
                Constants.matchId,
                live = false,
                started = false,
                date = null,
                time = null,
                prosCount = 0,
                teams = listOf()
            )
        )
        matchesRequest.stub {
            onBlocking { betMatches(Constants.betId) }.doReturn(matches)
        }

        tested.betMatches(Constants.betId)

        verify(matchesRequest, atLeastOnce()).betMatches(Constants.betId)
    }

    // MARK: - Tests: pros

    @Test
    fun requestFactoryProsRequest_shouldBeCalled_whenMatchProsCalled(): Unit = runBlocking {
        tested.matchPros(Constants.matchId)

        verify(requestFactory, atLeastOnce()).prosRequest()
    }

    @Test
    fun prosRequestMatchPros_shouldBeCalled_whenMatchProsCalled(): Unit = runBlocking {
        val pros = mutableListOf(
            BAPro(
                Constants.proId,
                null,
                false,
                null
            )
        )
        prosRequest.stub {
            onBlocking { matchPros(Constants.matchId) }.doReturn(pros)
        }

        tested.matchPros(Constants.matchId)

        verify(prosRequest).matchPros(Constants.matchId)
    }

    // MARK: - Tests: goals

    @Test
    fun requestFactoryGoalsRequest_shouldBeCalled_whenMatchGoalsCalled(): Unit = runBlocking {
        tested.matchGoals(Constants.matchId)

        verify(requestFactory).goalsRequest()
    }

    @Test
    fun goalsRequestMatchGoals_shouldBeCalled_whenMatchGoalsCalled(): Unit = runBlocking {
        val goals = mutableListOf(
            BAGoal(
                Constants.goalId,
                null,
                0,
                null
            )
        )
        goalsRequest.stub {
            onBlocking { matchGoals(Constants.matchId) }.doReturn(goals)
        }

        tested.matchGoals(Constants.matchId)

        verify(goalsRequest, atLeastOnce()).matchGoals(Constants.matchId)
    }
}
