package com.rednose.betsdk.core.client.models

interface Match {
    val id: String?
    val live: Boolean
    val started: Boolean
    val date: String?
    val time: String?
    val prosCount: Int
    val teams: List<Team>
}