package com.rednose.betsdk.core.client.models

data class BAColors(
    override val background: String?,
    override val primary: String?,
    override val secondary: String?,
    override val detail: String?
): Colors