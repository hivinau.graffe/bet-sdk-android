package com.rednose.betsdk.core.client.requests.factory

import com.rednose.betsdk.core.client.requests.BetsRequest
import com.rednose.betsdk.core.client.requests.GoalsRequest
import com.rednose.betsdk.core.client.requests.MatchesRequest
import com.rednose.betsdk.core.client.requests.ProsRequest

interface RequestFactory {
    fun betsRequest(): BetsRequest
    fun matchesRequest(): MatchesRequest
    fun prosRequest(): ProsRequest
    fun goalsRequest(): GoalsRequest
}