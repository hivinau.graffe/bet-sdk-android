package com.rednose.betsdk.core.client.models

interface Pro {
    val id: String?
    val label: String?
    val won: Boolean
    val type: String?
}