package com.rednose.betsdk.core.client.models

data class BATeam(override val id: String?,
                  override val name: String?,
                  override val image: String?,
                  override val colors: BAColors?
): Team