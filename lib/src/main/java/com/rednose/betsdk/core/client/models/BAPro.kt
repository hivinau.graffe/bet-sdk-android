package com.rednose.betsdk.core.client.models

import com.google.gson.annotations.SerializedName

data class BAPro(override val id: String?,
                 override val label: String?,
                 @SerializedName("is_won")
                 override val won: Boolean,
                 override val type: String?
): Pro