package com.rednose.betsdk.core.client.requests

import com.rednose.betsdk.core.client.models.BAPro
import retrofit2.http.GET
import retrofit2.http.Path

interface ProsRequest {

    @GET("matches/{id}/pros")
    suspend fun matchPros(@Path("id") id: String): List<BAPro>
}