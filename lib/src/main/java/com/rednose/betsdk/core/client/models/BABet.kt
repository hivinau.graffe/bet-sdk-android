package com.rednose.betsdk.core.client.models

import com.google.gson.annotations.SerializedName

data class BABet(override val id: String?,
                 @SerializedName("is_locked")
                 override val locked: Boolean,
                 @SerializedName("price_to_unlock")
                 override val priceToUnlock: Float,
                 @SerializedName("matches_count")
                 override val matchesCount: Int
): Bet