package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BAPro
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse

@RunWith(MockitoJUnitRunner::class)
class BAProTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val proString = """{"id":"4f53a78e-c9aa-4f86-870a-ddb700c530df","label":"Leeds ou match nul","is_won":false,"type":""}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfPro() {
        val pro = gson.fromJson(Constants.proString, BAPro::class.java)

        assertEquals(pro.id, "4f53a78e-c9aa-4f86-870a-ddb700c530df")
        assertEquals(pro.label, "Leeds ou match nul")
        assertFalse(pro.won)
        assertEquals(pro.type, "")
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsProString() {
        val pro = BAPro(
            "4f53a78e-c9aa-4f86-870a-ddb700c530df",
            "Leeds ou match nul",
            false,
            ""
        )

        val jsonString = gson.toJson(pro)

        assertEquals(jsonString, Constants.proString)
    }
}
