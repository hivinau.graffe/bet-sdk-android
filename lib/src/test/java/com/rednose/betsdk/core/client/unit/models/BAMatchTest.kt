package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BAMatch
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
class BAMatchTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val matchString = """{"id":"911a5456-5c7f-4f3e-9ab9-a85ca1a16cf6","is_live":true,"is_started":true,"date":"2021-02-19","time":"21:00","pros_count":0,"teams":[]}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfMatch() {
        val match = gson.fromJson(Constants.matchString, BAMatch::class.java)

        assertEquals(match.id, "911a5456-5c7f-4f3e-9ab9-a85ca1a16cf6")
        assertTrue(match.live)
        assertTrue(match.started)
        assertEquals(match.date, "2021-02-19")
        assertEquals(match.time, "21:00")
        assertEquals(match.prosCount, 0)
        assertTrue(match.teams.isEmpty())
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsMatchString() {
        val match = BAMatch(
            "911a5456-5c7f-4f3e-9ab9-a85ca1a16cf6",
            live = true,
            started = true,
            date = "2021-02-19",
            time = "21:00",
            prosCount = 0,
            teams = listOf()
        )

        val jsonString = gson.toJson(match)

        assertEquals(jsonString, Constants.matchString)
    }
}
