package com.rednose.betsdk.core.client.requests

import com.rednose.betsdk.core.client.models.BAMatch
import retrofit2.http.GET
import retrofit2.http.Path

interface MatchesRequest {

    @GET("bets/{id}/matches")
    suspend fun betMatches(@Path("id") id: String): List<BAMatch>
}