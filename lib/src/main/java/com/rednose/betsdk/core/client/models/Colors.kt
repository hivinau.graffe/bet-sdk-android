package com.rednose.betsdk.core.client.models

interface Colors {
    val background: String?
    val primary: String?
    val secondary: String?
    val detail: String?
}