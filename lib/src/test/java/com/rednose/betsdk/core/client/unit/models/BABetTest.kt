package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BABet
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
class BABetTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val betString = """{"id":"5cc34ff9-8116-4f0b-8983-8dc7dd5e2d59","is_locked":true,"price_to_unlock":0.5,"matches_count":2}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfBet() {
        val bet = gson.fromJson(Constants.betString, BABet::class.java)

        assertEquals(bet.id, "5cc34ff9-8116-4f0b-8983-8dc7dd5e2d59")
        assertTrue(bet.locked)
        assertEquals(bet.priceToUnlock, 0.5F)
        assertEquals(bet.matchesCount, 2)
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsBetString() {
        val bet = BABet(
            "5cc34ff9-8116-4f0b-8983-8dc7dd5e2d59",
            true,
            0.5F,
            2
        )

        val jsonString = gson.toJson(bet)

        assertEquals(jsonString, Constants.betString)
    }
}
