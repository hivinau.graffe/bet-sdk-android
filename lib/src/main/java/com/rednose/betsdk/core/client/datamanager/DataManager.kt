package com.rednose.betsdk.core.client.datamanager

interface DataManager {
    fun <T> create(service: Class<T>?): T
}