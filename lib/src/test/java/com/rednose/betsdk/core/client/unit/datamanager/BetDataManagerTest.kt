package com.rednose.betsdk.core.client.unit.datamanager

import com.rednose.betsdk.core.client.datamanager.BetDataManager
import com.rednose.betsdk.core.client.requests.BetsRequest
import com.rednose.betsdk.core.client.requests.GoalsRequest
import com.rednose.betsdk.core.client.requests.MatchesRequest
import com.rednose.betsdk.core.client.requests.ProsRequest
import com.rednose.betsdk.core.retrofit.Retrofit
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
class BetDataManagerTest {

    // MARK: - Constants

    class Constants {
        companion object {
            val betsRequest = BetsRequest::class.java
            val matchesRequest = MatchesRequest::class.java
            val prosRequest = ProsRequest::class.java
            val goalsRequest = GoalsRequest::class.java
        }
    }

    // MARK: - Private properties

    private lateinit var tested: BetDataManager
    private val retrofit = mock<Retrofit>()

    // MARK: - Lifecycle

    @Before
    fun setup() {
        tested = BetDataManager(retrofit)
    }

    // MARK: - Tests

    @Test
    fun retrofitCreateBetsRequest_shouldBeCalled_whenCreateCalled() {
        tested.create(Constants.betsRequest)

        verify(retrofit).create(Constants.betsRequest)
    }

    @Test
    fun retrofitCreateMatchesRequest_shouldBeCalled_whenCreateCalled() {
        tested.create(Constants.matchesRequest)

        verify(retrofit).create(Constants.matchesRequest)
    }

    @Test
    fun retrofitCreateProsRequest_shouldBeCalled_whenCreateCalled() {
        tested.create(Constants.prosRequest)

        verify(retrofit).create(Constants.prosRequest)
    }

    @Test
    fun retrofitCreateGoalsRequest_shouldBeCalled_whenCreateCalled() {
        tested.create(Constants.goalsRequest)

        verify(retrofit).create(Constants.goalsRequest)
    }
}