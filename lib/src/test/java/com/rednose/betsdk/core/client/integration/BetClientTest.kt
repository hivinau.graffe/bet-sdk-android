package com.rednose.betsdk.core.client.integration

import com.rednose.betsdk.core.client.BetClient
import com.rednose.betsdk.core.client.datamanager.BetDataManager
import com.rednose.betsdk.core.client.requests.factory.BetRequestFactory
import com.rednose.betsdk.core.retrofit.builder.RetrofitBuilderWrapper
import kotlinx.coroutines.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
class BetClientTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val serverPath = "http://192.168.86.118:8000"
            const val existingBetId = "5cc34ff9-8116-4f0b-8983-8dc7dd5e2d59"
            const val invalidBetId = "invalid bet id"
            const val existingMatchId = "adf27f8c-51eb-4fb1-8783-ca9e0e1ee30d"
            const val invalidMatchId = "invalid match id"
        }
    }

    // MARK: - Private properties

    private lateinit var tested: BetClient
    private val retrofitBuilder = RetrofitBuilderWrapper(Constants.serverPath)
    private val betDataManager =
        BetDataManager(retrofitBuilder.build())
    private val requestFactory =
        BetRequestFactory(betDataManager)

    // MARK: - Lifecycle

    @Before
    fun setup() {
        tested = BetClient(requestFactory)
    }

    // MARK: - Tests: bets

    @Test
    fun bets_shouldReturnNonEmptyBets(): Unit = runBlocking {
        assertTrue(tested.bets().isNotEmpty())
    }

    // MARK: - Tests: matches

    @Test
    fun betMatches_shouldReturnNonEmptyMatches_whenBetIdExists(): Unit = runBlocking {
        assertTrue(tested.betMatches(Constants.existingBetId).isNotEmpty())
    }

    @Test
    fun httpException_shouldBeThrown_whenBetMatchesCalledAndBetIdIsInvalid(): Unit = runBlocking {
        assertFailsWith<HttpException> {
            tested.betMatches(Constants.invalidBetId)
        }
    }

    // MARK: - Tests: pros

    @Test
    fun matchPros_shouldReturnNonEmptyMatches_whenMatchIdExists(): Unit = runBlocking {
        assertTrue(tested.matchPros(Constants.existingMatchId).isNotEmpty())
    }

    @Test
    fun httpException_shouldBeThrown_whenMatchProsCalledAndMatchIdIsInvalid(): Unit = runBlocking {
        assertFailsWith<HttpException> {
            tested.matchPros(Constants.invalidMatchId)
        }
    }

    // MARK: - Tests: goals

    @Test
    fun matchGoals_shouldReturnNonEmptyMatches_whenMatchIdExists(): Unit = runBlocking {
        assertTrue(tested.matchGoals(Constants.existingMatchId).isNotEmpty())
    }

    @Test
    fun httpException_shouldBeThrown_whenMatchGoalsCalledAndMatchIdIsInvalid(): Unit = runBlocking {
        assertFailsWith<HttpException> {
            tested.matchGoals(Constants.invalidMatchId)
        }
    }
}
