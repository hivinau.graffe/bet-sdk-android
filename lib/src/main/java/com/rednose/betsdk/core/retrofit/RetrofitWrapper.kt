package com.rednose.betsdk.core.retrofit

class RetrofitWrapper(private val retrofit: retrofit2.Retrofit): Retrofit {

    override fun <T> create(service: Class<T>?): T {
        return retrofit.create(service)
    }
}