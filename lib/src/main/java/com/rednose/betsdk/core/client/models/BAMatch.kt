package com.rednose.betsdk.core.client.models

import com.google.gson.annotations.SerializedName

data class BAMatch(override val id: String?,
                   @SerializedName("is_live")
                   override val live: Boolean,
                   @SerializedName("is_started")
                   override val started: Boolean,
                   override val date: String?,
                   override val time: String?,
                   @SerializedName("pros_count")
                   override val prosCount: Int,
                   override val teams: List<com.rednose.betsdk.core.client.models.BATeam>
): Match