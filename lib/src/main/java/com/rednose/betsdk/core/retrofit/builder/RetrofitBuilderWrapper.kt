package com.rednose.betsdk.core.retrofit.builder

import com.rednose.betsdk.core.retrofit.RetrofitWrapper
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilderWrapper(private val serverPath: String) : RetrofitBuilder {

    private val httpClient = OkHttpClient.Builder().build()
    private val gson = GsonBuilder().setLenient().create()
    private val gsonConverterFactory = GsonConverterFactory.create(gson)

    override fun build(): RetrofitWrapper {
        val retroFit = Retrofit.Builder()
            .baseUrl(serverPath)
            .client(httpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()

        return RetrofitWrapper(retroFit)
    }
}