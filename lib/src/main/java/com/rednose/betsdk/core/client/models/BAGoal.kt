package com.rednose.betsdk.core.client.models

import com.google.gson.annotations.SerializedName

data class BAGoal(override val id: String?,
                  override val scorer: String?,
                  override val time: Int,
                  @SerializedName("team_id")
                  override val teamId: String?
): Goal