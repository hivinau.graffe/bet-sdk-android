package com.rednose.betsdk.core.client.unit.models

import com.rednose.betsdk.core.client.models.BAColors
import com.rednose.betsdk.core.client.models.BATeam
import com.google.gson.Gson
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Test
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class BATeamTest {

    // MARK: - Constants

    class Constants {
        companion object {
            const val teamString = """{"id":"7c8e6980-938e-4f8e-92a6-b0bb303bae51","name":"Wolverhampton","image":"9be23c48a384a6b4.png","colors":{}}"""
        }
    }

    // MARK: - Private properties

    private val gson = Gson()

    // MARK: - Tests

    @Test
    fun gsonFromJsonString_shouldCreateAValidInstanceOfTeam() {
        val team = gson.fromJson(Constants.teamString, BATeam::class.java)

        assertEquals(team.id, "7c8e6980-938e-4f8e-92a6-b0bb303bae51")
        assertEquals(team.name, "Wolverhampton")
        assertEquals(team.image, "9be23c48a384a6b4.png")
    }

    @Test
    fun gsonToJsonString_shouldReturnConstantsTeamString() {
        val team = BATeam(
            "7c8e6980-938e-4f8e-92a6-b0bb303bae51",
            "Wolverhampton",
            "9be23c48a384a6b4.png",
            BAColors(null, null, null, null)
        )

        val jsonString = gson.toJson(team)

        assertEquals(jsonString, Constants.teamString)
    }
}
