import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val libraryVersion by extra("1.0.0")
val kotlinVersion by extra("1.5.0")
val jacocoVersion by extra("0.8.6")
val kotlinStdlib by extra("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
val androidxCore by extra("androidx.core:core-ktx:1.3.2")
val retrofit by extra("com.squareup.retrofit2:retrofit:2.9.0")
val gsonConverter by extra("com.squareup.retrofit2:converter-gson:2.9.0")
val kotlinxCoroutinesCore by extra("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.8")
val kotlinxCoroutinesAndroid by extra("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.8")
val junit by extra("junit:junit:4.12")
val kotlinTestJunit5 by extra("org.jetbrains.kotlin:kotlin-test-junit5")
val kotlinTestJunit5Specific by extra("org.jetbrains.kotlin:kotlin-test-junit5:$kotlinVersion")
val mockitoKotlin by extra("org.mockito.kotlin:mockito-kotlin:3.1.0")

plugins {
    id("com.gradle.plugin-publish") version "0.15.0"
    id("java-gradle-plugin")
    kotlin("jvm") version("1.5.0")
    id("java-library")
    id("maven-publish")
    id("signing")
    id("jacoco")
}

group = "com.rednose.betsdk"
version = libraryVersion

dependencies {
    implementation(kotlinStdlib)
    implementation(androidxCore)
    implementation(retrofit)
    implementation(gsonConverter)
    implementation(kotlinxCoroutinesCore)
    implementation(kotlinxCoroutinesAndroid)
    testImplementation(junit)
    testImplementation(kotlinTestJunit5)
    testImplementation(kotlinTestJunit5Specific)
    testImplementation(mockitoKotlin)
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = JavaVersion.VERSION_1_8.toString()
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = JavaVersion.VERSION_1_8.toString()
}

jacoco {
    toolVersion = jacocoVersion
}

tasks {
    test {
        useJUnit()
        finalizedBy(jacocoTestReport)
    }

    jacocoTestReport {
        dependsOn(test)
        reports {
            csv.isEnabled = false
            xml.isEnabled = true
            html.isEnabled = true
        }
    }

    register<JacocoReport>("applicationCodeCoverageReport") {
        sourceSets(sourceSets.main.get())
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}